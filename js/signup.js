import {verifyPassword, verifyUserName} from "./validateData.js"

const signupButton = document.getElementById("signup");
const userNameInput = document.getElementById("username");
const passwordInput = document.getElementById("password");
const strengthMeter = document.querySelector(".strength-meter");

init();

function showError(userNameInput, message) {
  const errorOutput = userNameInput.parentElement.querySelector(".text-danger");
  errorOutput.firstElementChild.textContent=message;
  errorOutput.style.visibility=(message === "") ? "hidden":"visible";

}

function validateUserName() {
  showError(userNameInput,verifyUserName(userNameInput.value));
}

function validatePassword() {
  //verifyPassword(passwordInput.value);
  const strength = 4;
  let color;
  switch (strength) {
    case 1: color="red";break;
    case 2: color="orange";break;
    case 3: color="yellow";break;
    case 4: color="green";break;
    default: color = "black";break;
  }
  const strengthElements=strengthMeter.children;
  for (let i = 0;i<strengthElements.length;i++) {
    if(i<strength) {
      strengthElements[i].style.backgroundColor = color;
    }else{
      strengthElements[i].style.backgroundColor = "lightgray";
    }


  }


}

function validateForm() {
  validateUserName();
  validatePassword();
}

function addEventHandlers() {
 // form.addEventListener("submit", validateForm);
  signupButton.addEventListener("click", validateForm);
}

function init() {
  addEventHandlers()
}

